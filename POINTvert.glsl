precision mediump float;

attribute vec4 a_PointPos;
attribute vec3 a_PointColor;

uniform mat4 u_ProjMat;
uniform mat4 u_WorldMat;

varying vec3 v_Color;

void main(void) {
    vec4 pos = u_ProjMat * u_WorldMat * vec4(a_PointPos.xyz, 1.0);

    gl_Position = pos;
    gl_PointSize = 1.0/pos.w*30.0;

    v_Color = a_PointColor;
}
