var webgl = {
    init: function(gl, callback) {
        gl.enable(gl.CULL_FACE);
        gl.getExtension("OES_standard_derivatives");
        gl.cullFace(gl.BACK);
        gl.clearColor(0, 0, 0, 1);
        gl.enable(gl.DEPTH_TEST);
        gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
        callback(gl);
    },
    createProgram: function(gl, vertUrl, fragUrl, callback) {
        function getUnifAttr(src) {
            var re = /^\s*(attribute|uniform)\s+(lowp|mediump|highp)?\s*([a-zA-Z_0-9]+)\s+([a-zA-Z_][a-zA-Z_0-9]*)\s*(\[\s*([0-9a-zA-Z]+)\s*\])?\s*;/gm;
            var results;
            var out = {
                unif: [],
                attr: []
            };
            while ((results = re.exec(src)) !== null) {
                if (results[1] == "uniform") {
                    out.unif.push({
                        full: results[0],
                        type: results[3],
                        name: results[4],
                        arrL: results[6]?parseInt(results[6]):undefined
                    });
                } else {
                    out.attr.push({
                        full: results[0],
                        type: results[3],
                        name: results[4],
                        arrL: results[6]?parseInt(results[6]):undefined
                    });
                }
            }
            return out;
        }

        function getTypeType(type) {
            if (type == "float") {
                return {type: 1, len: 1};
            }
            if (type == "vec2") {
                return {type: 2, len: 2};
            }
            if (type == "vec3") {
                return {type: 3, len: 3};
            }
            if (type == "vec4") {
                return {type: 4, len: 4};
            }
            if (type == "mat3") {
                return {type: 5, len: 9};
            }
            if (type == "mat4") {
                return {type: 6, len: 16};
            }
            if (type == "sampler2D") {
                return {type: 7, len: 1};
            }
            if (type == "samplerCube") {
                return {type: 8, len: 1};
            }
            if (type == "int") {
                return {type: 9, len: 1};
            }
            return 0;
        }

        function loadShader(program, url, type, reload) {
            httpGet(url, function(src) {
                gl.shaderSource(program[type], src);

                program[type].src = src;

                --done;

                if (!done) {
                    gl.compileShader(vert);
                    var compiled = gl.getShaderParameter(vert, gl.COMPILE_STATUS);
                    if (!compiled) {
                        var lastError = gl.getShaderInfoLog(vert);
                        console.log("*** Error compiling shader '" + vert + "':" + lastError);
                        gl.deleteShader(vert);
                        program.success = false;
                        callback(gl, program);
                        return;
                    }

                    gl.compileShader(frag);
                    compiled = gl.getShaderParameter(frag, gl.COMPILE_STATUS);
                    if (!compiled) {
                        var lastError = gl.getShaderInfoLog(frag);
                        console.log("*** Error compiling shader '" + frag + "':" + lastError);
                        gl.deleteShader(frag);
                        program.success = false;
                        callback(gl, program);
                        return;
                    }
                    if (!reload) {
                        gl.attachShader(program, vert);
                        gl.attachShader(program, frag);
                    }
                    gl.linkProgram(program);

                    var linked = gl.getProgramParameter(program, gl.LINK_STATUS);
                    if (!linked) {
                        var lastError = gl.getProgramInfoLog (program);
                        console.log("Error in program linking:" + lastError);
                        gl.deleteProgram(program);
                        program.success = false;
                        callback(gl, program);
                        return;
                    }
                    gl.useProgram(program);
                    var UA = getUnifAttr(program.vert.src+"\n"+program.frag.src);
                    if (!reload) {
                        for (var i=0; i<UA.unif.length; ++i) {
                            UA.unif[i].loc = gl.getUniformLocation(program, UA.unif[i].name);
                            UA.unif[i].typeType = getTypeType(UA.unif[i].type);
                            switch(UA.unif[i].typeType.type) {
                                case 1:
                                    UA.unif[i].setFunc = function(a){gl.uniform1fv(this.loc, a);};
                                    break;
                                case 2:
                                    UA.unif[i].setFunc = function(a){gl.uniform2fv(this.loc, a);};
                                    break;
                                case 3:
                                    UA.unif[i].setFunc = function(a){gl.uniform3fv(this.loc, a);};
                                    break;
                                case 4:
                                    UA.unif[i].setFunc = function(a){gl.uniform4fv(this.loc, a);};
                                    break;
                                case 5:
                                    UA.unif[i].setFunc = function(a){gl.uniformMatrix3fv(this.loc, false, a);};
                                    break;
                                case 6:
                                    UA.unif[i].setFunc = function(a){gl.uniformMatrix4fv(this.loc, false, a);};
                                    break;
                                case 7:
                                case 8:
                                case 9:
                                    UA.unif[i].setFunc = function(a){gl.uniform1i(this.loc, a);};
                                    break;
                                default:
                                    UA.unif[i].setFunc = function(a){gl.uniform1i(this.loc, a);};
                                    break;
                            }
                            program.unif[UA.unif[i].name] = UA.unif[i];
                        }

                        program.keys = [];

                        for (var i=0; i<UA.attr.length; ++i) {
                            UA.attr[i].typeType = getTypeType(UA.attr[i].type);

                            UA.attr[i].buff = gl.createBuffer();

                            UA.attr[i].index = i;

                            gl.bindAttribLocation(program, i, UA.attr[i].name);
                            gl.bindBuffer(gl.ARRAY_BUFFER, UA.attr[i].buff);
                            gl.enableVertexAttribArray(i);
                            gl.vertexAttribPointer(i, UA.attr[i].typeType.len, gl.FLOAT, false, 0, 0);

                            program.keys.push(UA.attr[i].name);

                            program.attr[UA.attr[i].name] = UA.attr[i];
                        }
                    } else {
                        var keys = Object.keys(program.unif);
                        for (var i=0; i<keys.length; ++i) {
                            program.unif[keys[i]].loc = gl.getUniformLocation(program, keys[i]);
                            if (program.unif[keys[i]].lastVal) {
                                program.setUnif(keys[i], program.unif[keys[i]].lastVal);
                            }
                        }
                        program.success = true;
                        reload(gl, program);
                        return;
                    }

                    program.success = true;
                    callback(gl, program);
                }
            });
        }

        var program = gl.createProgram();
        var vert = gl.createShader(gl.VERTEX_SHADER);
        var frag = gl.createShader(gl.FRAGMENT_SHADER);


        var done = 2;

        program.vert = vert;
        program.vert.url = vertUrl;
        program.vert.type = 0;

        program.frag = frag;
        program.frag.url = fragUrl;
        program.frag.type = 1;

        program.unif = {};
        program.attr = {};

        program.reload = function(reload) {
            done = 2;
            loadShader(program, vertUrl, "vert", reload?reload:function(){done=1;});
            loadShader(program, fragUrl, "frag", reload?reload:function(){done=1;});
        }

        program.bufferData = function(name, data) {
            gl.bindBuffer(gl.ARRAY_BUFFER, program.attr[name].buff);
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        }

        program.bufferSubData = function(name, data, start, end) {
            gl.bindBuffer(gl.ARRAY_BUFFER, program.attr[name].buff);
            gl.bufferSubData(gl.ARRAY_BUFFER, data.BYTES_PER_ELEMENT * start, data.subarray(start, end));
        }

        program.setUnif = function(name, val) {
            program.unif[name].setFunc(val);
            program.unif[name].lastVal = val;
        }

        program.use = function() {
            gl.useProgram(program);
            var i = 0;
            for (var key in program.attr) {
                gl.bindBuffer(gl.ARRAY_BUFFER, program.attr[key].buff);
                gl.vertexAttribPointer(i, program.attr[key].typeType.len, gl.FLOAT, false, 0, 0);
                ++i;
            }
        }

        loadShader(program, vertUrl, "vert");
        loadShader(program, fragUrl, "frag");
    },

    loadTexture: function(gl, url) {
        var texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);

        var level = 0;
        var internalFormat = gl.RGBA;
        var width = 1;
        var height = 1;
        var border = 0;
        var srcFormat = gl.RGBA;
        var srcType = gl.UNSIGNED_BYTE;
        var pixel = new Uint8Array([0, 0, 255, 255]);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                      width, height, border, srcFormat, srcType,
                      pixel);

        var image = new Image();
        image.onload = function() {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                          srcFormat, srcType, image);

            if ((image.width & (image.width - 1) == 0) && (image.width & (image.width - 1) == 0)) {
                gl.generateMipmap(gl.TEXTURE_2D);
            } else {
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }
        };
        image.src = url;

        return texture;
    }


}

//# sourceURL=webgl1.js