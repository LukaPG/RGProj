function begin(gl) {
    webgl.createProgram(gl, "DALvert.glsl", "DALfrag.glsl", function(gl, program) {
        webgl.createProgram(gl, "POINTvert.glsl", "POINTfrag.glsl", function(gl, pointProgram) {
            webgl.createProgram(gl, "GUIvert.glsl", "GUIfrag.glsl", function(gl, guiProgram) {
                var perspMatrix = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
                window.onresize = function() {
                    var height = window.innerHeight;
                    var width =  window.innerWidth;

                    gl.canvas.height = height;
                    gl.canvas.width = width;

                    var aspect = width / height;

                    createPerspective(radians(50), aspect, 0.001, 1000000000, perspMatrix);

                    program.use();
                    program.setUnif("u_ProjMat", perspMatrix);

                    pointProgram.use();
                    pointProgram.setUnif("u_ProjMat", perspMatrix);

                    guiProgram.use();
                    guiProgram.setUnif("u_ScreenSize", [width, height]);

                    gl.viewport(0, 0, width, height);
                };
                gl.clear(gl.COLOR_BUFFER_BIT);

                function keydown(e) {
                    keysPressed[e.keyCode] = 1;
                }

                function keyup(e) {
                    keysPressed[e.keyCode] = 0;
                }

                function mousemove(e) {
                    if (document.pointerLockElement === gl.canvas) {
                        rotY -= e.movementX/10;
                        rotX -= e.movementY/10;
                        rotX = rotX <= -90 ? -90 : rotX >= 90 ? 90 : rotX;
                    }
                }

                gl.canvas.onclick    = gl.canvas.requestPointerLock;
                gl.canvas.ondblclick = function() {
                    program.reload(window.onresize);
                    pointProgram.reload(window.onresize);
                }

                document.onkeydown   = keydown;
                document.onkeyup     = keyup;
                document.onmousemove = mousemove;

                pro = program;
                pointPro = pointProgram;
                GL = gl;

                program.use();

                var albedo    = webgl.loadTexture(gl, "rustediron2_albedo.png");
                var normal    = webgl.loadTexture(gl, "rustediron2_normal.png");
                var metallic  = webgl.loadTexture(gl, "rustediron2_metallic.png");
                var roughness = webgl.loadTexture(gl, "rustediron2_roughness.png");
                var emissive  = webgl.loadTexture(gl, "rustediron2_emissive.png");
                var ao        = webgl.loadTexture(gl, "rustediron2_ao.png");

                // var albedo    = webgl.loadTexture(gl, "cratered_rock_albedo.png");
                // var normal    = webgl.loadTexture(gl, "cratered_rock_normal.png");
                // var metallic  = webgl.loadTexture(gl, "cratered_rock_metallic.png");
                // var roughness = webgl.loadTexture(gl, "cratered_rock_roughness.png");
                // var emissive  = webgl.loadTexture(gl, "cratered_rock_emissive.png");
                // var ao        = webgl.loadTexture(gl, "cratered_rock_ao.png");

                // var albedo    = webgl.loadTexture(gl, "cratered_rock_lava_albedo.png");
                // var normal    = webgl.loadTexture(gl, "cratered_rock_lava_normal.png");
                // var metallic  = webgl.loadTexture(gl, "cratered_rock_lava_metallic.png");
                // var roughness = webgl.loadTexture(gl, "cratered_rock_lava_roughness.png");
                // var emissive  = webgl.loadTexture(gl, "cratered_rock_lava_emissive.png");
                // var ao        = webgl.loadTexture(gl, "cratered_rock_lava_ao.png");

                // var albedo    = webgl.loadTexture(gl, "redbricks2b_albedo.png");
                // var normal    = webgl.loadTexture(gl, "redbricks2b_normal.png");
                // var metallic  = webgl.loadTexture(gl, "redbricks2b_metallic.png");
                // var roughness = webgl.loadTexture(gl, "redbricks2b_roughness.png");
                // var emissive  = webgl.loadTexture(gl, "redbricks2b_emissive.png");
                // var ao        = webgl.loadTexture(gl, "redbricks2b_ao.png");

                // var albedo    = webgl.loadTexture(gl, "plasticpattern1_albedo.png");
                // var normal    = webgl.loadTexture(gl, "plasticpattern1_normal.png");
                // var metallic  = webgl.loadTexture(gl, "plasticpattern1_metallic.png");
                // var roughness = webgl.loadTexture(gl, "plasticpattern1_roughness.png");
                // var emissive  = webgl.loadTexture(gl, "plasticpattern1_emissive.png");
                // var ao        = webgl.loadTexture(gl, "plasticpattern1_ao.png");

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, albedo);

                gl.activeTexture(gl.TEXTURE1);
                gl.bindTexture(gl.TEXTURE_2D, normal);

                gl.activeTexture(gl.TEXTURE2);
                gl.bindTexture(gl.TEXTURE_2D, metallic);

                gl.activeTexture(gl.TEXTURE3);
                gl.bindTexture(gl.TEXTURE_2D, roughness);

                gl.activeTexture(gl.TEXTURE4);
                gl.bindTexture(gl.TEXTURE_2D, emissive);

                gl.activeTexture(gl.TEXTURE5);
                gl.bindTexture(gl.TEXTURE_2D, ao);


                program.setUnif("u_albedoMap",    0);
                program.setUnif("u_normalMap",    1);
                program.setUnif("u_metallicMap",  2);
                program.setUnif("u_roughnessMap", 3);
                program.setUnif("u_emissiveMap",  4);
                program.setUnif("u_aoMap",        5);

                var camPos = [0, -1.5, 4, 1];

                var upVec      = [ 0,-1, 0, 0];
                var downVec    = [ 0, 1, 0, 0];

                var rightVec   = [-1, 0, 0, 0];
                var leftVec    = [ 1, 0, 0, 0];

                var forwardVec = [ 0, 0, 1, 0];
                var backVec    = [ 0, 0,-1, 0];

                var numOfLights = 4;

                var PI2 = Math.PI*2;

                var lightPos         = new Float32Array(4 * numOfLights);
                var lightPosBuffer   = [];
                var lightPosSA       = new Float32Array(4 * numOfLights);
                var lightPosSABuffer = [];
                var lightColor       = new Float32Array(3 * numOfLights);
                var lightPower       = new Float32Array(numOfLights);

                var keysPressed = new Int8Array(256);

                var rotX =  0;
                var rotY =  0;

                var t = performance.now()/1000;
                var ts = t;
                var tt = 0;
                var fr = 0;

                var rotationMatrix = new Float32Array(16);
                var translationMatrix = new Float32Array(16);
                var scaleMatrix = new Float32Array(16);

                var cameraRotX = new Float32Array(16);
                var cameraRotY = new Float32Array(16);
                var cameraTran = new Float32Array(16);
                var camera = new Float32Array(16);

                rotateX(radians(rotX), cameraRotX);
                rotateY(radians(rotY), cameraRotY);
                translate(camPos, cameraTran);

                transpose(cameraTran, cameraTran);


                multiplyMat4Mat4(cameraTran, cameraRotY, camera);
                multiplyMat4Mat4(camera,     cameraRotX, camera);

                program.setUnif("u_WorldMat", camera);

                scale([2, 1, 2], scaleMatrix);
                translate([0, 0, -2, 0], translationMatrix);
                multiplyMat4Mat4(translationMatrix, scaleMatrix, scaleMatrix);

                translate([0, 0, -4, 0], translationMatrix);

                for (var i=0; i<numOfLights; ++i) {
                    rotateY(t+i*PI2/numOfLights, rotationMatrix);

                    multiplyMat4Mat4(translationMatrix, rotationMatrix, rotationMatrix);

                    lightPos[i*4 + 0] = 0;
                    lightPos[i*4 + 1] = -0.25;
                    lightPos[i*4 + 2] = -0.7071;
                    lightPos[i*4 + 3] = 1;

                    lightColor[i*3 + 0] = 1;
                    lightColor[i*3 + 1] = 0.931345411760339;
                    lightColor[i*3 + 2] = 0.8715508191543596;

                    lightPower[i] = 1/Math.pow(2, 3);

                    lightPosBuffer.push(lightPos.subarray(i*4,i*4+4));

                    lightPosSABuffer.push(lightPosSA.subarray(i*4,i*4+4));

                    multiplyMat4Vec4(rotationMatrix, lightPosBuffer[i], lightPosSABuffer[i]);
                }


                program.setUnif("u_lightPos",   lightPosSA);
                program.setUnif("u_lightColor", lightColor);
                program.setUnif("u_lightPower", lightPower);





                program.setUnif("u_camPos", camPos);

                var testPos = new Float32Array(36);
                testPos[ 0] = -0.5;
                testPos[ 1] = -0.5;
                testPos[ 2] =  0.5;

                testPos[ 3] =  0.5;
                testPos[ 4] = -0.5;
                testPos[ 5] = -0.5;

                testPos[ 6] = -0.5;
                testPos[ 7] = -0.5;
                testPos[ 8] = -0.5;


                testPos[ 9] =  0.5;
                testPos[10] = -0.5;
                testPos[11] = -0.5;

                testPos[12] = -0.5;
                testPos[13] = -0.5;
                testPos[14] =  0.5;

                testPos[15] =  0.5;
                testPos[16] = -0.5;
                testPos[17] =  0.5;



                testPos[18] = -0.5;
                testPos[19] = -0.5;
                testPos[20] =  0.5;

                testPos[21] =  0.5;
                testPos[22] = -0.5;
                testPos[23] = -0.5;

                testPos[24] = -0.5;
                testPos[25] = -0.5;
                testPos[26] = -0.5;


                testPos[27] =  0.5;
                testPos[28] = -0.5;
                testPos[29] = -0.5;

                testPos[30] = -0.5;
                testPos[31] = -0.5;
                testPos[32] =  0.5;

                testPos[33] =  0.5;
                testPos[34] = -0.5;
                testPos[35] =  0.5;

                for (var i=0; i<6; ++i) {
                    multiplyMat4Vec3(scaleMatrix, testPos.subarray(i*3, i*3+3), testPos.subarray(i*3, i*3+3));
                }

                scale([2, 1, 2], scaleMatrix);
                translate([0, 0, -4, 0], translationMatrix);
                multiplyMat4Mat4(translationMatrix, scaleMatrix, scaleMatrix);

                for (var i=6; i<12; ++i) {
                    multiplyMat4Vec3(scaleMatrix, testPos.subarray(i*3, i*3+3), testPos.subarray(i*3, i*3+3));
                }



                var testNorm = new Float32Array(36);
                testNorm[ 0] = 0;
                testNorm[ 1] = 1;
                testNorm[ 2] = 0;

                testNorm[ 3] = 0;
                testNorm[ 4] = 1;
                testNorm[ 5] = 0;

                testNorm[ 6] = 0;
                testNorm[ 7] = 1;
                testNorm[ 8] = 0;


                testNorm[ 9] = 0;
                testNorm[10] = 1;
                testNorm[11] = 0;

                testNorm[12] = 0;
                testNorm[13] = 1;
                testNorm[14] = 0;

                testNorm[15] = 0;
                testNorm[16] = 1;
                testNorm[17] = 0;



                testNorm[18] = 0;
                testNorm[19] = 1;
                testNorm[20] = 0;

                testNorm[21] = 0;
                testNorm[22] = 1;
                testNorm[23] = 0;

                testNorm[24] = 0;
                testNorm[25] = 1;
                testNorm[26] = 0;


                testNorm[27] = 0;
                testNorm[28] = 1;
                testNorm[29] = 0;

                testNorm[30] = 0;
                testNorm[31] = 1;
                testNorm[32] = 0;

                testNorm[33] = 0;
                testNorm[34] = 1;
                testNorm[35] = 0;



                var testUV = new Float32Array(24);
                testUV[ 0] = 0;
                testUV[ 1] = 1;

                testUV[ 2] = 1;
                testUV[ 3] = 0;

                testUV[ 4] = 0;
                testUV[ 5] = 0;


                testUV[ 6] = 1;
                testUV[ 7] = 0;

                testUV[ 8] = 0;
                testUV[ 9] = 1;

                testUV[10] = 1;
                testUV[11] = 1;



                testUV[12] = 0;
                testUV[13] = 1;

                testUV[14] = 1;
                testUV[15] = 0;

                testUV[16] = 0;
                testUV[17] = 0;


                testUV[18] = 1;
                testUV[19] = 0;

                testUV[20] = 0;
                testUV[21] = 1;

                testUV[22] = 1;
                testUV[23] = 1;


                program.bufferData("a_VertPos",  testPos);
                program.bufferData("a_VertNorm", testNorm);
                program.bufferData("a_TexCoord", testUV);

                pointProgram.use();


                pointProgram.bufferData("a_PointPos", lightPosSA);
                pointProgram.bufferData("a_PointColor", lightColor);

                pointProgram.setUnif("u_WorldMat", camera);

                pointProgram.reload();

                guiProgram.use();

                var str1 = "ČŠŽčšžćđß¤,.:;_<>€|\\-?*+=˝\"'~¸¨!#$%&/()ˇ^˘°˛`˙´0123456789÷[]";
                var str2 = "the five boxing wizards jump quickly";
                var str3 = "THE FIVE BOXING WIZARDS JUMP QUICKLY";

                var guiARR = new Float32Array(1000000);

                var fontSize = 12;

                var fontHeight = fontSize*1.5;

                var guiLENS = 0;
                guiLENS = gui.text(str1, fontSize, fontSize, 3*fontHeight, guiARR, guiLENS);
                guiLENS = gui.text(str2, fontSize, fontSize, 2*fontHeight, guiARR, guiLENS);
                guiLENS = gui.text(str3, fontSize, fontSize, 1*fontHeight, guiARR, guiLENS);

                guiProgram.bufferData("a_VertPos", guiARR);

                guiProgram.setUnif("u_Color", [1.0, 1.0, 1.0]);

                guiProgram.reload();

                window.onresize();

                function render() {
                    // if(a = gl.getError()) {
                    //     console.error('Error:'+a);
                    // }
                    t = performance.now();
                    var tp = t - ts;
                    ts = t;

                    program.use();

                    for (var i=0; i<numOfLights; ++i) {
                        rotateY(t/1000 + i*PI2/numOfLights, rotationMatrix);

                        multiplyMat4Mat4(translationMatrix, rotationMatrix, rotationMatrix);

                        multiplyMat4Vec4(rotationMatrix, lightPosBuffer[i], lightPosSABuffer[i]);
                    }

                    if (keysPressed[65]) {//a
                        multiplyMat4Vec4(cameraRotY, leftVec, supportVec3);
                        scalarProductVec4(supportVec3, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    if (keysPressed[68]) {//d
                        multiplyMat4Vec4(cameraRotY, rightVec, supportVec3);
                        scalarProductVec4(supportVec3, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    if (keysPressed[87]) {//w
                        multiplyMat4Vec4(cameraRotY, forwardVec, supportVec3);
                        scalarProductVec4(supportVec3, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    if (keysPressed[83]) {//s
                        multiplyMat4Vec4(cameraRotY, backVec, supportVec3);
                        scalarProductVec4(supportVec3, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    if (keysPressed[32]) {//space
                        scalarProductVec4(upVec, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    if (keysPressed[16]) {//shift
                        scalarProductVec4(downVec, tp*0.005, supportVec3);
                        addVec4(supportVec3, camPos, camPos);
                    }

                    rotateX(radians(rotX), cameraRotX);
                    rotateY(radians(rotY), cameraRotY);
                    translate(camPos, cameraTran);

                    transpose(cameraTran, cameraTran);


                    multiplyMat4Mat4(cameraTran, cameraRotY, camera);
                    multiplyMat4Mat4(camera,     cameraRotX, camera);

                    program.setUnif("u_camPos", camPos);

                    program.setUnif("u_WorldMat", camera);

                    program.setUnif("u_lightPos", lightPosSA);

                    gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
                    gl.drawArrays(gl.TRIANGLES, 0, testPos.length/3);

                    gl.flush();

                    pointProgram.use();

                    pointProgram.setUnif("u_WorldMat", camera);

                    pointProgram.bufferData("a_PointPos", lightPosSA);
                    pointProgram.bufferData("a_PointColor", lightColor);

                    gl.drawArrays(gl.POINTS, 0, numOfLights);
                    gl.drawArrays(gl.LINE_LOOP, 0, numOfLights);


                    gl.flush();


                    gl.disable(gl.CULL_FACE);
                    gl.disable(gl.DEPTH_TEST);

                    var guiLEN = guiLENS;
                    guiLEN = gui.text("FPS: "+(Math.round(1/tp*100000)/100), fontSize, fontSize, gl.canvas.height-fontHeight, guiARR, guiLEN);
                    guiLEN = gui.text("Vertices: "+guiLEN, fontSize, fontSize, gl.canvas.height-fontHeight*2, guiARR, guiLEN);
                    guiProgram.bufferData("a_VertPos", guiARR.subarray(0, guiLEN));

                    guiProgram.use();

                    gl.drawArrays(gl.TRIANGLES, 0, guiLEN/2);

                    gl.enable(gl.CULL_FACE);
                    gl.enable(gl.DEPTH_TEST);

                    gl.flush();

                    window.requestAnimationFrame(render);
                }

                window.requestAnimationFrame(render);
            });
        });
    });
}

//# sourceURL=core.js