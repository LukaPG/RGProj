var gui = {
    text: function(string, size, x, y, o, i) {
        for (var j=0; j<string.length; ++j) {
            var charCode = string[j];
            if (" \f\n\r\t\v\u00A0\u2028\u2029".indexOf(charCode) >= 0) {
                continue;
            }
            var data = gui.font[charCode];
            var tris;
            if (data) {
                tris = data;
            } else {
                tris = gui.font["\u0000"];
            }
            for (var k=0; k<tris.length; k+=2) {
                var nx = tris[k+0]*size + x + j*gui.font.width*size;
                var ny = tris[k+1]*size + y;
                o[i++] = nx;
                o[i++] = ny;
            }
        }
        return i;
    }
};