var supportVec0 = [0, 0, 0, 0];
var supportVec1 = [0, 0, 0, 0];
var supportVec2 = [0, 0, 0, 0];
var supportVec3 = [0, 0, 0, 0];

var supportMat0 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var supportMat1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var supportMat2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var supportMat3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

function multiplyMat4Mat4(ma, mb, mo) {
    var ma0  = ma[ 0];
    var ma1  = ma[ 1];
    var ma2  = ma[ 2];
    var ma3  = ma[ 3];

    var ma4  = ma[ 4];
    var ma5  = ma[ 5];
    var ma6  = ma[ 6];
    var ma7  = ma[ 7];

    var ma8  = ma[ 8];
    var ma9  = ma[ 9];
    var ma10 = ma[10];
    var ma11 = ma[11];

    var ma12 = ma[12];
    var ma13 = ma[13];
    var ma14 = ma[14];
    var ma15 = ma[15];


    var mb0  = mb[ 0];
    var mb1  = mb[ 1];
    var mb2  = mb[ 2];
    var mb3  = mb[ 3];

    var mb4  = mb[ 4];
    var mb5  = mb[ 5];
    var mb6  = mb[ 6];
    var mb7  = mb[ 7];

    var mb8  = mb[ 8];
    var mb9  = mb[ 9];
    var mb10 = mb[10];
    var mb11 = mb[11];

    var mb12 = mb[12];
    var mb13 = mb[13];
    var mb14 = mb[14];
    var mb15 = mb[15];

    mo[ 0] = ma0*mb0  + ma1*mb4  + ma2*mb8   + ma3 *mb12;
    mo[ 1] = ma0*mb1  + ma1*mb5  + ma2*mb9   + ma3 *mb13;
    mo[ 2] = ma0*mb2  + ma1*mb6  + ma2*mb10  + ma3 *mb14;
    mo[ 3] = ma0*mb3  + ma1*mb7  + ma2*mb11  + ma3 *mb15;

    mo[ 4] = ma4*mb0  + ma5*mb4  + ma6*mb8   + ma7 *mb12;
    mo[ 5] = ma4*mb1  + ma5*mb5  + ma6*mb9   + ma7 *mb13;
    mo[ 6] = ma4*mb2  + ma5*mb6  + ma6*mb10  + ma7 *mb14;
    mo[ 7] = ma4*mb3  + ma5*mb7  + ma6*mb11  + ma7 *mb15;

    mo[ 8] = ma8*mb0  + ma9*mb4  + ma10*mb8  + ma11*mb12;
    mo[ 9] = ma8*mb1  + ma9*mb5  + ma10*mb9  + ma11*mb13;
    mo[10] = ma8*mb2  + ma9*mb6  + ma10*mb10 + ma11*mb14;
    mo[11] = ma8*mb3  + ma9*mb7  + ma10*mb11 + ma11*mb15;

    mo[12] = ma12*mb0 + ma13*mb4 + ma14*mb8  + ma15*mb12;
    mo[13] = ma12*mb1 + ma13*mb5 + ma14*mb9  + ma15*mb13;
    mo[14] = ma12*mb2 + ma13*mb6 + ma14*mb10 + ma15*mb14;
    mo[15] = ma12*mb3 + ma13*mb7 + ma14*mb11 + ma15*mb15;
}

function scalarProductVec4(v, s, vo) {
    vo[0] = v[0]*s;
    vo[1] = v[1]*s;
    vo[2] = v[2]*s;
    vo[3] = v[3]*s;
}

function addVec4(va, vb, vo) {
    vo[0] = va[0] + vb[0];
    vo[1] = va[1] + vb[1];
    vo[2] = va[2] + vb[2];
    vo[3] = va[3] + vb[3];
}

function transpose(m, mo) {
    var m0  = m[ 0];
    var m1  = m[ 1];
    var m2  = m[ 2];
    var m3  = m[ 3];

    var m4  = m[ 4];
    var m5  = m[ 5];
    var m6  = m[ 6];
    var m7  = m[ 7];

    var m8  = m[ 8];
    var m9  = m[ 9];
    var m10 = m[10];
    var m11 = m[11];

    var m12 = m[12];
    var m13 = m[13];
    var m14 = m[14];
    var m15 = m[15];

    // mo[ 0] = m0;
    mo[ 1] = m4;
    mo[ 2] = m8;
    mo[ 3] = m12;

    mo[ 4] = m1;
    // mo[ 5] = m5;
    mo[ 6] = m9;
    mo[ 7] = m13;

    mo[ 8] = m2;
    mo[ 9] = m6;
    // mo[10] = m10;
    mo[11] = m14;

    mo[12] = m3;
    mo[13] = m7;
    mo[14] = m11;
    // mo[15] = m15;
}

function translate(v, mo) {
    mo[ 0] = 1;
    mo[ 1] = 0;
    mo[ 2] = 0;
    mo[ 3] = v[0];

    mo[ 4] = 0;
    mo[ 5] = 1;
    mo[ 6] = 0;
    mo[ 7] = v[1];

    mo[ 8] = 0;
    mo[ 9] = 0;
    mo[10] = 1;
    mo[11] = v[2];

    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0;
    mo[15] = 1;
}

function rotateX(rad, mo) {
    var c = Math.cos(rad);
    var s = Math.sin(rad);
    mo[ 0] = 1;
    mo[ 1] = 0;
    mo[ 2] = 0;
    mo[ 3] = 0;

    mo[ 4] = 0;
    mo[ 5] = c;
    mo[ 6] = -s;
    mo[ 7] = 0;

    mo[ 8] = 0;
    mo[ 9] = s;
    mo[10] = c;
    mo[11] = 0;

    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0  ;
    mo[15] = 1;
}

function rotateY(rad, mo) {
    var c = Math.cos(rad);
    var s = Math.sin(rad);
    mo[ 0] = c;
    mo[ 1] = 0;
    mo[ 2] = s;
    mo[ 3] = 0;

    mo[ 4] = 0;
    mo[ 5] = 1;
    mo[ 6] = 0;
    mo[ 7] = 0;

    mo[ 8] = -s;
    mo[ 9] = 0;
    mo[10] = c;
    mo[11] = 0;

    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0;
    mo[15] = 1;
}

function rotateZ(rad, mo) {
    var c = Math.cos(rad);
    var s = Math.sin(rad);
    mo[ 0] = 1;
    mo[ 1] = c;
    mo[ 2] = -s;
    mo[ 3] = 0;

    mo[ 4] = 0;
    mo[ 5] = S;
    mo[ 6] = c;
    mo[ 7] = 0;

    mo[ 8] = 0;
    mo[ 9] = 0;
    mo[10] = 1;
    mo[11] = 0;

    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0;
    mo[15] = 1;
}

function scale(v, mo) {
    mo[ 0] = v[0];
    mo[ 1] = 0;
    mo[ 2] = 0;
    mo[ 3] = 0;
    mo[ 4] = 0;
    mo[ 5] = v[1];
    mo[ 6] = 0;
    mo[ 7] = 0;
    mo[ 8] = 0;
    mo[ 9] = 0;
    mo[10] = v[2];
    mo[11] = 0;
    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0;
    mo[15] = 1;
}

function multiplyMat4Vec4(m, v, vo) {
    var v0 = v[0];
    var v1 = v[1];
    var v2 = v[2];
    var v3 = v[3];
    vo[0] = m[ 0]*v0 + m[ 1]*v1 + m[ 2]*v2 + m[ 3]*v3;
    vo[1] = m[ 4]*v0 + m[ 5]*v1 + m[ 6]*v2 + m[ 7]*v3;
    vo[2] = m[ 8]*v0 + m[ 9]*v1 + m[10]*v2 + m[11]*v3;
    vo[3] = m[12]*v0 + m[13]*v1 + m[14]*v2 + m[15]*v3;
}

function multiplyMat4Vec3(m, v, vo) {
    var v0 = v[0];
    var v1 = v[1];
    var v2 = v[2];
    vo[0] = m[ 0]*v0 + m[ 1]*v1 + m[ 2]*v2 + m[ 3];
    vo[1] = m[ 4]*v0 + m[ 5]*v1 + m[ 6]*v2 + m[ 7];
    vo[2] = m[ 8]*v0 + m[ 9]*v1 + m[10]*v2 + m[11];
}

function lengthVec4(v) {
    return Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

function normalizeVec4(v, vo) {
    var length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])||1;
    vo[0] = v[0]/length;
    vo[1] = v[1]/length;
    vo[2] = v[2]/length;
    vo[3] = v[3];
}

function normalizeVec3(v, vo) {
    var length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])||1;
    vo[0] = v[0]/length;
    vo[1] = v[1]/length;
    vo[2] = v[2]/length;
}

function dotVec3(va, vb) {
    return va[0]*vb[0] + va[1]*vb[1] + va[2]*vb[2];
}

function crossVec3(va, vb, vo) {
    var va0 = va[0];
    var va1 = va[1];
    var va2 = va[2];


    var vb0 = vb[0];
    var vb1 = vb[1];
    var vb2 = vb[2];

    vo[0] = va1*vb2 - va2*vb1;
    vo[1] = va2*vb0 - va0*vb2;
    vo[2] = va0*vb1 - va1*vb0;
}

function crossVec4(va, vb, vo) {
    var va0 = va[0];
    var va1 = va[1];
    var va2 = va[2];


    var vb0 = vb[0];
    var vb1 = vb[1];
    var vb2 = vb[2];

    vo[0] = va1*vb2 - va2*vb1;
    vo[1] = va2*vb0 - va0*vb2;
    vo[2] = va0*vb1 - va1*vb0;
    vo[3] = va[3]*vb[3];
}

function lookAt(pos, target, up, mo) {
    zaxis = normal(At - Eye)
    xaxis = normal(cross(Up, zaxis))
    yaxis = cross(zaxis, xaxis)

    supportVec0[0] = target[0] - pos[0];
    supportVec0[1] = target[1] - pos[1];
    supportVec0[2] = target[2] - pos[2];
    normalizeVec3(supportVec0, supportVec0);
    crossVec3(up, supportVec0, supportVec1);
    normalizeVec3(supportVec1, supportVec1);
    crossVec3(supportVec0, supportVec1, supportVec2);
    mo[ 0] = supportVec0[0];
    mo[ 1] = supportVec1[0];
    mo[ 2] = supportVec2[0];
    mo[ 3] = - pos[0]*supportVec0[0] - pos[1]*supportVec1[0] - pos[2]*supportVec2[0];
    mo[ 4] = supportVec0[1];
    mo[ 5] = supportVec1[1];
    mo[ 6] = supportVec2[1];
    mo[ 7] = - pos[0]*supportVec0[1] - pos[1]*supportVec1[1] - pos[2]*supportVec2[1];
    mo[ 8] = supportVec0[2];
    mo[ 9] = supportVec1[2];
    mo[10] = supportVec2[2];
    mo[11] = - pos[0]*supportVec0[2] - pos[1]*supportVec1[2] - pos[2]*supportVec2[2];
    mo[12] = 0;
    mo[13] = 0;
    mo[14] = 0;
    mo[15] = 1;
}

function createPerspective(fov, aspect, znear, zfar, mo) {
    mo[6] = (znear * Math.tan(fov))/2;
    mo[5] = -mo[6];
    mo[3] = (mo[6] - mo[5]);
    mo[2] = (mo[6] - mo[5]);
    mo[1] = (zfar - znear);
    mo[10] = (-(zfar + znear) / mo[1]);
    mo[14] = (-2 * (zfar * znear) / mo[1]);
    mo[0] = (2 * znear / mo[3]);
    mo[0] = (mo[0] / aspect);
    mo[5] = (2 * znear / mo[2]);
    mo[0] = mo[0];
    mo[1] = 0;
    mo[2] = 0;
    mo[3] = 0;
    mo[4] = 0;
    mo[5] = mo[5];
    mo[6] = 0;
    mo[7] = 0;
    mo[8] = 0;
    mo[9] = 0;
    mo[10] = mo[10];
    mo[11] = -1;
    mo[12] = 0;
    mo[13] = 0;
    mo[14] = mo[14];
    mo[15] = 0;
}

function radians(deg) {
    return deg*Math.PI/180;
}

function degrees(rad) {
    return rad/Math.PI*180;
}

function getTangent(pos, uv, to) {
    var edge1x = pos[3] - pos[0];
    var edge1y = pos[4] - pos[1];
    var edge1z = pos[5] - pos[2];

    var edge2x = pos[6] - pos[0];
    var edge2y = pos[7] - pos[1];
    var edge2z = pos[8] - pos[2];

    var dUV1x = uv[2] - uv[0];
    var dUV1y = uv[3] - uv[1];

    var dUV2x = uv[4] - uv[0];
    var dUV2y = uv[5] - uv[1];

    var f = 1 / (dUV1x * dUV2y - dUV1y * dUV2x);

    var tx = f * (dUV2y * edge1x - dUV1y * edge2x);
    var ty = f * (dUV2y * edge1y - dUV1y * edge2y);
    var tz = f * (dUV2y * edge1z - dUV1y * edge2z);

    var l = Math.sqrt(tx*tx + ty*ty + tz*tz);

    to[0] = tx/l;
    to[1] = ty/l;
    to[2] = tz/l;
}

//# sourceURL=math.js