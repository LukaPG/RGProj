precision mediump float;

uniform vec3 u_Color;

// varying highp vec2 v_TexCoord;

void main() {
    gl_FragColor = vec4(u_Color, 1.0);
}