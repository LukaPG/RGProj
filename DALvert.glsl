precision mediump float;

attribute vec3 a_VertPos;
attribute vec3 a_VertNorm;
attribute vec2 a_TexCoord;

uniform mat4 u_ProjMat;
uniform mat4 u_WorldMat;

varying highp vec2 v_TexCoord;
varying vec3 v_VertPos;
varying vec3 v_VertNorm;

void main(void) {
    vec4 pos = u_ProjMat * u_WorldMat * vec4(a_VertPos, 1.0);
    gl_Position = pos;

    vec3 Normal  = normalize(a_VertNorm);

    v_TexCoord = a_TexCoord;
    v_VertPos  = a_VertPos;
    v_VertNorm = Normal;
}
