precision mediump float;

attribute vec2 a_VertPos;
// attribute highp vec2 a_TexCoord;

uniform vec2 u_ScreenSize;

// varying vec2 v_TexCoord;

void main() {
    gl_Position = vec4((a_VertPos/u_ScreenSize)*2.0 - 1.0, 1.0, 1.0);
}