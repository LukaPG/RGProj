function httpGet(url, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.responseType = "text";
    xmlHttp.onreadystatechange = function() {
        /*Chromium returns status as 0 if loaded from local file*/
        if (xmlHttp.readyState == 4 && (xmlHttp.status == 200 || xmlHttp.status == 0)) {
            callback(xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
}

function getGl(canvas) {
    var gl = canvas.getContext("webgl", {
                                        antialias: false,
                                        failIfMajorPerformanceCaveat: true});
    if (gl) {
        gl.canvas = canvas;
        gl.version = 1;
        return gl;
    }
    gl = canvas.getContext("webgl", {
                                    antialias: false,
                                    failIfMajorPerformanceCaveat: true});
    if (gl) {
        gl.canvas = canvas;
        gl.version = 1;
        return gl;
    }
    gl = canvas.getContext("experimental-webgl");
    if (gl) {
        gl.canvas = canvas;
        gl.version = 1;
        return gl;
    }
    return false;
}

window.onload = function() {
    var canvas = document.getElementById("canvas");
    var gl = getGl(canvas);
    if (!gl) {
        console.log("WebGL not supported! Aborting.");
        alert("Your browser does not support WebGL.");
    }
    if (gl.version == 2) {
        webgl2.init(gl);
    }
    if (gl.version == 1) {
        webgl.init(gl, begin);
    }
}

//# sourceURL=init.js