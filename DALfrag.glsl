#extension GL_OES_standard_derivatives:enable
#define WITH_NORMALMAP_UNSIGNED
#define WITH_NORMALMAP_GREEN_UP

#define NUMOFLIGHTS 16

precision mediump float;

uniform vec4      u_lightPos[NUMOFLIGHTS];
uniform vec3      u_lightColor[NUMOFLIGHTS];
uniform float     u_lightPower[NUMOFLIGHTS];

uniform sampler2D u_albedoMap;
uniform sampler2D u_normalMap;
uniform sampler2D u_metallicMap;
uniform sampler2D u_roughnessMap;
uniform sampler2D u_emissiveMap;
uniform sampler2D u_aoMap;

uniform vec4      u_camPos;
uniform mat4      u_WorldMat;


varying highp vec2 v_TexCoord;
varying vec3       v_VertPos;
varying vec3       v_VertNorm;

const float PI = 3.14159265359;

const float iPI = 1.0/PI;

mat3 cotangent_frame(vec3 N, vec3 p, vec2 uv) {
    // get edge vectors of the pixel triangle
    vec3 dp1  = dFdx(p);
    vec3 dp2  = dFdy(p);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    // solve the linear system
    vec3 dp2perp = cross(dp2, N);
    vec3 dp1perp = cross(N, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt(max(dot(T,T), dot(B,B)));
    return mat3( T * invmax, B * invmax, N );
}

vec3 perturb_normal(vec3 N, vec3 V, vec2 texcoord) {
    // assume N, the interpolated vertex normal and
    // V, the view vector (vertex to eye)
    vec3 map = texture2D(u_normalMap, texcoord).xyz;
#ifdef WITH_NORMALMAP_UNSIGNED
    map = map * 255.0/127.0 - 128.0/127.0;
#endif
#ifdef WITH_NORMALMAP_2CHANNEL
    map.z = sqrt( 1.0 - dot( map.xy, map.xy ) );
#endif
#ifdef WITH_NORMALMAP_GREEN_UP
    map.y = -map.y;
#endif
    mat3 TBN = cotangent_frame( N, -V, texcoord );
    return normalize( TBN * map );
}



// float DistributionGGX(vec3 N, vec3 H, float roughness);
// float GeometrySchlickGGX(float NdotV, float roughness);
// float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
// vec3  fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness);

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}
float DistributionGGX(vec3 N, vec3 H, float r4) {
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = r4;
    float denom = (NdotH2 * (r4 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
float GeometrySchlickGGX(float NdotV, float k) {
    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(float NdotV, float NdotL, float k) {
    float ggx2  = GeometrySchlickGGX(NdotV, k);
    float ggx1  = GeometrySchlickGGX(NdotL, k);

    return ggx1 * ggx2;
}

void main() {
    vec3  albedo    = pow(texture2D(u_albedoMap, v_TexCoord).rgb, vec3(2.2));
    float metallic  = texture2D(u_metallicMap, v_TexCoord).r;
    float roughness = texture2D(u_roughnessMap, v_TexCoord).r;
    float emissive  = texture2D(u_emissiveMap, v_TexCoord).r;
    float ao        = texture2D(u_aoMap, v_TexCoord).r;

    float r2 = roughness*roughness;
    float r4 = r2*r2;
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;



    vec3 V = normalize( - u_camPos.xyz - v_VertPos);
    vec3 N = perturb_normal(normalize(v_VertNorm), V, v_TexCoord);

    float NdotV = max(dot(N, V), 0.0);


    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    float nMetallic = 1.0 - metallic;
    vec3 nMV = vec3(nMetallic);

    vec3 albedoiPI = albedo * iPI;

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < NUMOFLIGHTS; ++i) {
        vec3 lightPos = u_lightPos[i].xyz;

        // calculate per-light radiance
        vec3  L           = normalize(lightPos - v_VertPos);
        vec3  H           = normalize(V + L);
        float distance    = length(lightPos - v_VertPos);
        float attenuation = u_lightPower[i] / (distance * distance);
        vec3  radiance    = u_lightColor[i] * attenuation;

        float NdotL = max(dot(N, L), 0.0);

        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, r4);
        float G   = GeometrySmith(NdotV, NdotL, k);
        vec3  F   = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 kS = F;
        vec3 kD = nMV - kS * nMetallic;

        vec3 nominator    = NDF * G * F;
        float denominator = 4.0 * NdotV * NdotL + 0.001;
        vec3 specular     = nominator / denominator;

        // add to outgoing radiance Lo
        Lo += (kD * albedoiPI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.00) * albedo * ao;
    vec3 color = mix(ambient + Lo, albedo, emissive);

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    gl_FragColor = vec4(color, 1.0);
    // gl_FragColor = vec4(texture2D(u_albedoMap, v_TexCoord).rgb, 1.0);
    // gl_FragColor = vec4(N, 1.0);
    // gl_FragColor = vec4(vec3(metallic), 1.0);
    // gl_FragColor = vec4(vec3(roughness), 1.0);
    // gl_FragColor = vec4(vec3(emissive), 1.0);
    // gl_FragColor = vec4(vec3(ao), 1.0);
    // gl_FragColor = vec4(1.0);
}
